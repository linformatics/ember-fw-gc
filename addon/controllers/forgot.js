import Controller from '@ember/controller';
import {inject as service} from '@ember/service';

import {validator, buildValidations} from 'ember-cp-validations';
import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';

const ForgotValidator = buildValidations({
    email: [
        validator('presence', {
            presence: true,
            message: 'Please enter an email.'
        }),
        validator('format', {
            allowBlank: true,
            type: 'email',
            message: 'Email address is not valid.'
        })
    ]
});

/**
 * The Forgot Page is the page which will be displayed when you click on the forgot button on the login page. It is a page with a single textbox, email and two buttons: back and
 * Send Reset Email. It will simply send a reset email button with a link to the email put in the textbox if there is a user set up for that email.
 *
 * Everything is set up for you, from the route to the controller to the template. There is nothing you need to do to configure this (so long as you already imported the router as instructed
 * in the setup instructions for [Ember FW GC](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-gc/setup)).
 *
 * @class ForgotPage
 * @module Pages
 */

export default Controller.extend(ForgotValidator, ValidationMixin, {
    email: '',
    resetUrl: '',

    config: service(),
    ajax: service(),
    notifications: service(),

    actions: {
        sendResetEmail() {
            this.get('notifications').closeAllNotifications('forgot');

            let forgotUrl = this.get('config').formGCUrl('session', 'forgot');

            return this.validate().then(() => {
                return this.get('ajax').post(forgotUrl, {
                    data: {
                        email: this.get('email'),
                        url: this.get('resetUrl')
                    }
                }).then((response) => {
                    if (response && !response.error) {
                        this.get('notifications').showSuccess('If your account is in our records, an email has been sent to you.', 'forgot', true);
                    } else {
                        this.get('notifications').showError(response.message || 'An error occured.', 'forgot', true);
                    }
                }).catch((e) => {
                    this.get('notifications').showError('An error occurred.', 'forgot');

                    throw e;
                });
            });
        }
    }
});
