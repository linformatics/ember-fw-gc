import Controller from '@ember/controller';
import {inject as service} from '@ember/service';
import RSVP from 'rsvp';

/**
 * The Login Page is the page which loads in to every app when the user has not been authenticated. It is a very simple looking page which has a username and password textbox, with a
 * login button and a "forgot password" button. The only other thing on this page is the "Joke of the Day", which if there are jokes set up in the database appears underneath the
 * password text field. The very simple function of this page is just to login, so it implements the group-control authenticator and properly authenticates the user.
 *
 * Everything is set up for you, from the route to the controller to the template. There is nothing you need to do to configure this (so long as you already imported the router as instructed
 * in the setup instructions for [Ember FW GC](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-gc/setup)).
 *
 * @class LoginPage
 * @module Pages
 */
export default Controller.extend({
    queryParams: ['setdept'],
    session: service(),
    notifications: service(),
    jokes: service(),
    config: service(),
    ajax: service(),

    jokeOfTheDay: null,

    init() {
        this._super(...arguments);
        let url = this.get('config').formGCUrl('jokes', 'getJoke');
        this.get('ajax').request(url).then((data) => {
            this.set('jokeOfTheDay', data.joke);
            this.get('jokes').setJoke(data.joke);
        });
    },
    // passed in by parent
    dept: null,

    actions: {
        login() {
            let data = this.get('model').getProperties('identification', 'password');

            this.get('notifications').closeAllNotifications('login');
            return RSVP.resolve(this.get('model').validate()).then(() => {
                return RSVP.resolve(this.get('session').authenticate('authenticator:group-control', data.identification, data.password, this.get('setdept'))).catch((e) => {
                    if (e.payload) {
                        if (e.payload.message) {
                            // if the error is in JSON, use its given message
                            this.get('notifications').showError(e.payload.message, 'login');
                        } else {
                            // Otherwise just give the first line of the payload
                            this.get('notifications').showError(e.payload.split('\n')[0], 'login');
                        }
                    } else if (e.message) {
                        // On the chance that is missing, print the first line of the error message
                        this.get('notifications').showError(e.message.split('\n')[0], 'login');
                    }
                    throw e;
                });
            });
        },
        showAnswer() {
            this.get('notifications').showSuccess(this.get('jokeOfTheDay.answer'), 'fixed', true);
            let url = this.get('config').formGCUrl('jokes', 'increaseCount');
            this.get('ajax').put(url, {data: {id: this.get('jokeOfTheDay.id')}});
        }
    }
});
