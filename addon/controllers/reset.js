import Controller from '@ember/controller';
import {inject as service} from '@ember/service';
import {
    ForbiddenError,
    isBadRequestError
} from 'ember-ajax/errors';

import {validator, buildValidations} from 'ember-cp-validations';
import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';

const ResetValidator = buildValidations({
    password: [
        validator('presence', {
            presence: true,
            allowBlank: true,
            message: 'You must supply a password.'
        })
    ],
    confirmPassword: [
        validator('confirmation', {
            on: 'password',
            message: 'Passwords do not match.'
        })
    ]
});
/**
 * The Reset Page is the page which will only be displayed if you sent yourself a reset email and click the link in the email. The page loads with two textfields
 * password and confirm password. If you type both of them in and they match (even if it is the same password as the last password), your password will be reset,
 * and then the user will be automatically authenticated. It uses a randomly generated "token" query parameter to distinguish which user's password you are attempting to
 * reset. The only way to make this token is to use the forgot password page.
 *
 * Everything is set up for you, from the route to the controller to the template. There is nothing you need to do to configure this (so long as you already imported the router as instructed
 * in the setup instructions for [Ember FW GC](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-gc/setup)).
 *
 * @class ResetPage
 * @module Pages
 */
export default Controller.extend(ResetValidator, ValidationMixin, {
    password: '',
    confirmPassword: '',

    queryParams: ['token'],
    token: '',

    notifications: service(),
    config: service(),
    session: service(),
    ajax: service(),

    actions: {
        resetPassword() {
            let resetUrl = this.get('config').formGCUrl('session', 'reset');

            this.get('notifications').closeAllNotifications('reset');

            return this.validate().then(() => {
                return this.get('ajax').put(resetUrl, {
                    data: {
                        new: this.get('password'),
                        confirm: this.get('confirmPassword'),
                        token: this.get('token')
                    }
                }).then((response) => {
                    let data;
                    if (response && response.id) {
                        data = {user: response.id, pass: this.get('password')};

                        return this.get('session').authenticate('authenticator:group-control', data.user, data.pass).then(() => {
                            this.get('notifications').closeAllNotifications('reset');
                        }).catch((e) => {
                            if (e instanceof ForbiddenError) {
                                this.get('notifications').showError('You do not have permission to access this application', 'reset');
                            }
                        });
                    } else {
                        this.get('notifications').showError(response.message || 'An error occured.', 'reset');
                    }
                }).catch((error) => {
                    if (isBadRequestError(error)) {
                        this.get('notifications').showError(error.errors[0].detail.message, 'reset');
                    } else {
                        this.get('notifications').showError('An error occured', 'reset');
                    }
                });
            });
        }
    }
});
