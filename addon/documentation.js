/* Below is the main documentation for the addon. This file is only for documentation, it doesn't actually do anything. */
/**
 * The primary purpose of ember-fw-gc is to integrate Group Control Elements into a Group Control dependent app.
 * This documentation is designed just to be the API docs, which can aid in developers. For a more comprehensive
 * guide to Ember FW GC, see our [Ember FW GC](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-gc) docs.
 *
 * @module Introduction
 * @main Introduction
 */
/**
  * Pages defined by Ember FW GC.
  *
  * @module Pages
  * @main Pages
  */
/**
 * Models defined by Ember FW GC.
 *
 * @module Models
 * @main Models
*/
/**
 * Components defined by Ember FW GC.
 *
 * @module Components
 * @main Components
*/
/**
 * Helpers defined by Ember FW GC.
 *
 * @module Helpers
 * @main Helpers
*/
/**
 * Services defined by Ember FW GC.
 *
 * @module Services
 * @main Services
*/
/**
 * Utils defined by Ember FW GC.
 *
 * @module Utils
 * @main Utils
*/
/**
 * Mixins defined by Ember FW GC.
 *
 * @module Mixins
 * @main Mixins
*/
/**
 * Miscellaneous catch-all module for things which don't neetly fit into the other modules.
 *
 * @module Miscellaneous
 * @main Miscellaneous
 */
