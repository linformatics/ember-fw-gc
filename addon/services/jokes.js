import Service from '@ember/service';
import {inject} from '@ember/service';
/**
 * This service allows access to the joke which has been generated/fetched by the login page and displayed there.
 * It does not handle the actual fetching from Group Control of the joke, but merely sets or gets the joke, which
 * the Login page loaded. It should probably not be used externally to ember-fw-gc.
 *
 * @public
 * @class JokesService
 * @extends Ember.Service
 * @module Services
 */

// TODO: currently it seems that the jokes service actually does nothing except look pretty. Probably see if you want
// to remove it. Most of the functionality is implemented in the login controller itself.
export default Service.extend({
    /**
     * Internal joke model. Easiest to use the getter and setters provided rather than calling
     * directly.
     * @protected
     * @property joke
     * @type {DS.Model}
     */
    joke: null,

    /**
     * This function will take a model which is returned from the network request and set the internal joke property
     *
     * @method setJoke
     * @param {DS.Model} joke The joke model that was returned from the network request to Group Control
     */

    setJoke(joke) {
        this.set('joke', joke);
    },

    /**
     * This function returns the current internal joke model, which has been previously set.
     * @method getJoke
     * @return {DS.Model} The model of the joke which is currently being displayed on the login screen.
     */

    getJoke() {
        return this.get('joke');
    }
});
