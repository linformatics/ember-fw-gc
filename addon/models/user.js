import Model from 'ember-data/model';
import {computed} from '@ember/object';
import attr from 'ember-data/attr';
import {isEmpty} from '@ember/utils';

/**
 * This user model is used in order to access the GC User model within the app, which depends on Group Control. This has abundant use cases, and all of our current apps use
 * the user model. To make sure that any changes to the GC User model are accommodated within the apps, you must import this user model rather than defining your own. That can be done
 * by making a `model/user.js` file in your app, and setting it to the following export statment:
 *
 * ```js
 * export {default} from '@bennerinformatics/ember-fw-gc/models/user';
 * ```
 *
 * Additionally, in order to properly make this model work, you must accompany it with the proper adapter (so that it doesn't look for a User Serverside Model in your own app).
 * See [fw-app](../classes/FwAppAdapter.html) if you need more information about that.
 *
 * After you have done these two steps, you can use user as any other Ember model, and it will properly get the data from Group Control.
 *
 * @public
 * @class UserModel
 * @module Models
 */

export default Model.extend({
    // keys from group control
    /**
     * This property is saved in Group Control and is a string of the first name of the user.
     * @property nameFirst
     * @type {String}
     */
    nameFirst: attr('string'),
    /**
     * This property is saved in Group Control and is a string of the last name of the user.
     * @property nameLast
     * @type {String}
     */
    nameLast: attr('string'),
    /**
     * This property is saved in Group Control and is a string of the preferred name (nickname) of the user.
     * @property namePref
     * @type {String}
     */
    namePref: attr('string'),
    /**
     * This property is saved in Group Control and a boolean of whether the user is in the current department
     * (as a regular user, not a guest user). This is used to filter dropdowns so not all users show up in them
     * (you should almost always filter all your dropdowns by inDept = true, unless you have a clear reason for wanting
     * all possible users).
     *
     * @property inDept
     * @type {String}
     * @default true
     */
    inDept: attr('boolean', {defaultValue: true}),

    // sort name uses preferred first name
    /**
     * nameSort is a computed property, which returns the preferred first name (so `namePref` if it is defined or `nameFirst` if it isn't).
     * This is used by the `nameFull` property.
     *
     * @property nameSort
     * @type {Computed}
     */
    nameSort: computed('namePref', 'nameFirst', function () {
        return this.get('namePref') || this.get('nameFirst');
    }),

    // full combines preferred name and last
    /**
     * nameFull is a computed property, which returns the full name of the user ({nameSort nameLast}). If either nameSort or nameLast is empty,
     * it will return the userId instead.
     *
     * @property nameFull
     * @type {Computed}
     */
    nameFull: computed('nameSort', 'nameLast', 'id', function () {
        if (isEmpty(this.get('nameSort')) || isEmpty(this.get('nameLast'))) {
            return this.get('id');
        }
        return `${this.get('nameSort')} ${this.get('nameLast')}`;
    }),

    // variations used in nameMatches and other user lookups
    /**
     * nameFullLower is a computed property used in `nameMatches` and other lookups, which is simply the lowercase version of `nameFull`.
     *
     * @property nameFullLower
     * @type {Computed}
     */
    nameFullLower: computed('nameFull', function() {
        return this.get('nameFull').toLowerCase();
    }),
    /**
     * nameFormalLower is a computed property used in `nameMatches` and other lookups, which is simply the lowercase version of the user's formal name (ie {nameFirst nameLast}
     * regardless if there is a namePref defined or not).
     *
     * @property nameFormalLower
     * @type {Computed}
     */
    nameFormalLower: computed('nameFirst', 'nameLast', function() {
        return `${this.get('nameFirst')} ${this.get('nameLast')}`.toLowerCase();
    }),

    /**
     * Checks if the passed in name matches this user. Used for search functionality.
     *
     * @method nameMatches
     * @param  {string} name Name to check
     * @return {boolean}     True if the name matches this user's id, full name, or full name without preferred name
     */
    nameMatches(name) {
        if (isEmpty(name)) {
            return false;
        }

        // cleanup whitespace
        name = name.trim().toLowerCase().replace(/\s{2,}/g, ' ');
        return name === this.get('id')
            || name === this.get('nameFullLower')
            || name === this.get('nameFormalLower');
    }
});
