import Mixin from '@ember/object/mixin';
import {computed} from '@ember/object';
import {isEmpty} from '@ember/utils';
import {inject as injectService} from '@ember/service';

/**
 * For Ember deprecation notice, [click here](https://deprecations.emberjs.com/id/ember-component-is-visible/). Basic mixin to be implemented in various components
 * that enables showing or hiding it based on auth permissions
 *
 * To use in a component:
 * ```javascript
 * // app/components/some-component.js
 * import Ember from 'ember';
 * import AuthComponentMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-component';
 *
 * export default Ember.Component.extend(AuthComponentMixin, {
 *     // your code here....
 * });
 * ```
 * This is used in the [AuthBlock](AuthBlock.html) and [AuthButton](AuthButton.html) components. You are able to
 * use this in your own components too, but it is likely that you will never call this component directly.
 *
 * @public
 * @class AuthComponentMixin
 * @module Mixins
 * @deprecated Ember Component isVisible has been deprecated by Ember. Use has-role or has-department helper instead.
 */
export default Mixin.create({
    /**
     * This is the default visibility of the component which uses this mixin. This is used if neither `perm`
     * nor `department` are set.
     *
     * @public
     * @property defaultVisibility
     * @type {Boolean}
     * @default true
     */
    defaultVisibility: true,

    /**
     * Permission value. This will be passed directly to the [match](MatchUtil.html) function.
     *
     * @public
     * @property perm
     * @type {String|Array}
     */
    perm: '',

    /**
     * If supplied, the mixin checks this against a user's departments.
     *
     * @public
     * @property department
     * @type {String}
     */
    department: '',

    /**
     * If supplied, this will be checked against the user's roles using
     * the [match](MatchUtil.html) function. If it passes,
     * it will ignore the `department` property.
     *
     * @public
     * @property override
     * @type {String}
     */
    override: '',

    currentUser: injectService(),
    session: injectService(),

    /**
     * This sets the isVisible property, computed based on all the other properties passed in.
     * @private
     * @property isVisible
     * @type {Computed Boolean}
     */

    isVisible: computed('session.isAuthenticated', 'user.roles', 'perm', 'department', 'defaultVisibility', function () {
        let user = this.get('currentUser');

        if (!this.get('session.isAuthenticated')) {
            return false;
        }

        if (this.get('override') && user.match(this.get('override'))) {
            return true;
        }

        if (isEmpty(this.get('perm'))) {
            if (this.get('department')) {
                return user.checkDepartment(this.get('department'));
            }
            return this.get('defaultVisibility');
        }

        return user.match(this.get('perm')) && (this.get('department') ? user.checkDepartment(this.get('department')) : true);
    })
});
