import Mixin from '@ember/object/mixin';
import {inject} from '@ember/service';
import {isNone, typeOf} from '@ember/utils';
import RSVP from 'rsvp';

/**
 * A mixin designed to be used by the application route to change the user's department based on the query parameter `setdept` if they have access to that department.
 * In practice, this means that after this mixin is properly set up in application route, passing `?setdept=info` to the end of any url in any app, will change the department
 * to Informatics before loading the page.
 *
 * This is how it would be set up in `routes/application.js`:
 *
 * ```javascript
 * import Ember from 'ember';
 * import DepartmentTransitionMixin from '@bennerinformatics/ember-fw-gc/mixins/department-transition';
 *
 * export default Ember.Route.extend(DepartmentTransitionMixin, {
 *     // route code here
 * });
 * ```
 *
 * This mixin is incorporated into the application route by default in the Generator FW, so it should already be in your app. If it is not, please add it to `routes/application.js`
 * as described.
 *
 * @public
 * @class DepartmentTransitionMixin
 * @extends Ember.Mixin
 * @module Mixins
 */
export default Mixin.create({
    currentUser: inject(),
    session: inject(),

    beforeModel(transition) {
        // if not authenticated, there is no point changing departments, we have none
        if (this.get('session.isAuthenticated')) {
            // this fetch is essentially read only, so it will still exist after transition
            let params = transition.router._lastQueryParams || transition.queryParams;
            if (!isNone(params)) {
                let {setdept} = params;
                if (!isNone(setdept)) {
                    let user = this.get('currentUser');
                    // ensure it could be a department, we are actually changing, and the user can use this department
                    if (typeOf(setdept) === 'string'
                        && !user.checkDepartment(setdept)
                        && user.get('departments').includes(setdept)) {

                        return RSVP.all([
                            user.changeDepartment(setdept),
                            this._super(transition)
                        ]);
                    }
                }
            }
        }
        return this._super(transition);
    }
});
