import Mixin from '@ember/object/mixin';
import {inject as service} from '@ember/service';

/**
 * Mixin that the application controller needs to use in order
 * to be able to use certain things like the login modal functionality
 *
 * Usage: (in controllers/application.js)
 * ```javascript
 * import Ember from 'ember';
 * import ApplicationControllerMixin from '@bennerinformatics/ember-fw-gc/mixins/application-controller';
 *
 * export default Ember.Controller.extend(ApplicationControllerMixin, {
 *     // more code here
 * });
 * ```
 *
 * This should be already added for you in the application controller, as this is added by default by the Generator FW.
 *
 * @public
 * @class ApplicationControllerMixin
 * @extends Ember.Mixin
 * @module Mixins
 */
export default Mixin.create({
    showLoginModal: false,
    attemptedTransition: null,

    session: service(),

    init() {
        this._super(...arguments);
        this.get('session').on('re-login', this, function (attemptedTransition) {
            this.send('openLoginModal', attemptedTransition);
        });
    },

    actions: {
        openLoginModal(attemptedTransition = null) {
            if (attemptedTransition) {
                this.set('attemptedTransition', attemptedTransition);
            }
            this.set('showLoginModal', true);
        },

        closeLoginModal() {
            this.set('attemptedTransition', null);
            this.set('showLoginModal', false);
        }
    }
});
