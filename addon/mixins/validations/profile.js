import {validator, buildValidations} from 'ember-cp-validations';

export default buildValidations({
    id: validator('presence', {
        presence: true,
        allowBlank: true,
        message: 'Please specify a username.'
    }),
    nameFirst: [
        validator('presence', {
            presence: true,
            allowBlank: true,
            message: 'Please specify a first name.'
        }),
        validator('length', {
            allowBlank: true,
            max: 45,
            message: 'First name is too long.'
        })
    ],
    nameLast: [
        validator('presence', {
            presence: true,
            allowBlank: true,
            message: 'Please specify a last name.'
        }),
        validator('length', {
            allowBlank: true,
            max: 45,
            message: 'Last name is too long.'
        })
    ],
    namePref: validator('length', {
        allowBlank: true,
        max: 45,
        message: 'Preferred name is too long.'
    }),
    email: [
        validator('presence', {
            presence: true,
            allowBlank: true,
            message: 'Please specify an email.'
        }),
        validator('format', {
            type: 'email',
            allowBlank: true,
            message: 'Email is invalid.'
        })
    ]
});
