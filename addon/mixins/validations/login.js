import {validator, buildValidations} from 'ember-cp-validations';

export default buildValidations({
    identification: validator('presence', {
        presence: true,
        ignoreBlank: true,
        message: 'You must supply a username/email.'
    }),
    password: validator('presence', {
        presence: true,
        ignoreBlank: true,
        message: 'You must supply a password.'
    })
});
