import BaseValidator from 'ember-cp-validations/validators/base';

export default BaseValidator.extend({
    validate(value, options, model) {
        return !model.get('wrongPassword') || 'Password supplied is invalid.';
    }
});
