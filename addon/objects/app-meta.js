import EmberObject, {computed} from '@ember/object';

/**
 * This object defines the properties contained in an app meta object. AppMeta is essentially the information that is saved about the app in the `fw.json` file.
 * This object stores that information. It is used in tandem with the [AppMeta Service](AppMetaService.html), and should probably not be called directly. But this is
 * the internal "app-meta" object, so you can know when using the service, what properties are available for each app.
 *
 * @public
 * @class AppMeta
 * @extends EmberObject
 * @module Miscellaneous
 */
export default EmberObject.extend({
    /**
     * App ID
     * @property appId
     * @type {String}
     */

    /**
     * Name of the app
     * @property name
     * @type {String}
     */

    /**
     * App version
     * @property version
     * @type {String}
     */

    /**
     * Git repository
     * @property repo
     * @type {String}
     */

    /**
     * App URL relative to webroot
     * @property url
     * @type {Array}
     */

    /**
     * Computed URL with no trailing slash. Used by `FwAppLink`.
     * @property urlClean
     * @type {Computed String}
     */
    urlClean: computed('url', function() {
        return this.get('url').replace(/\/$/g, '');
    }),

    /**
     * Relative app logo path
     * @property logo
     * @type {String}
     */

    /**
     * App logo relative to the webroot. Computed.
     * @property
     * @type {Computed String}
     */
    logoUrl: computed('logo', 'urlClean', function() {
        return `${this.get('urlClean')}/${this.get('logo').replace(/^\//g, '')}`;
    }),

    /**
     * Relative api path
     * @property api
     * @type {String}
     */

    /**
     * API path relative to the webroot. computed.
     * @property apiUrl
     * @type {Computed String}
     */
    apiUrl: computed('api', 'urlClean', function() {
        return `${this.get('urlClean')}/${this.get('api').replace(/^\//g, '')}`;
    })

    /**
     * Author of the app.
     * @property author
     * @type {Array}
     */

    /**
     * Array of current maintainers of the app.
     * @property maintainers
     * @type {Array}
     */

    /**
     * Array of previous maintainers for the app.
     * @property previousMaintainers
     * @type {Array}
     */
});
