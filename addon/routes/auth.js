import BaseRoute from '@bennerinformatics/ember-fw/routes/base';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import AuthCheckMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-check';
import PermissionsMixin from '@bennerinformatics/ember-fw-gc/mixins/permissions';

/**
 * Route that includes several of the auth-related mixins already included,
 * made for convenience.
 *
 * Usage: (in a routes file)
 * ```javascript
 * import AuthRoute from '@bennerinformatics/ember-fw-gc/routes/auth';
 *
 * export default AuthRoute.extend({
 *     // your code here...
 * });
 * ```
 *
 * This AuthRoute is extended by both the [AuthRoute](AuthRoute.html) and the [RestrictedRoute](RestrictedRoute.html), both of which are routes you can access within
 * your app. Thus all the mixins incorporated into this route is used by both of those as well. You should hardly ever import this AuthRoute instead of the AuthRoute accessible
 * directly.
 *
 * @public
 * @class AuthRoute-Addon
 * @extends BaseRoute
 * @uses AuthenticatedRouteMixin
 * @uses AuthCheckMixin
 * @uses PermissionsMixin
 * @module Miscellaneous
 */
export default BaseRoute.extend(AuthenticatedRouteMixin, AuthCheckMixin, PermissionsMixin);
