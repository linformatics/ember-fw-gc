import EmberObject from '@ember/object';
import {inject as injectService} from '@ember/service';
import {getOwner} from '@ember/application';
import RSVP from 'rsvp';
import AuthRoute from '@bennerinformatics/ember-fw-gc/routes/auth';
import AuthRouteMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-route';

import ProfileValidation from '@bennerinformatics/ember-fw-gc/mixins/validations/profile';
import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';

const CurrentUserModel = EmberObject.extend(ProfileValidation, ValidationMixin);

export default AuthRoute.extend(AuthRouteMixin, {
    title: 'User Profile',

    user: injectService('current-user'),
    config: injectService(),
    ajax: injectService(),

    model() {
        let url = this.get('config').formGCUrl.bind(this.get('config'));
        let get = this.get('ajax').request.bind(this.get('ajax'));

        return RSVP.all([
            get(url('users', this.get('user.userId'))),
            get(url('departments', {limit: 'current'})),
            get(url('settings', {app: this.get('config.appId')}))
        ]).then(([{user}, {departments}, {settings}]) => {
            return {
                user: CurrentUserModel.create(getOwner(this).ownerInjection(), user),
                departments,
                settings
            };
        });
    }
});
