import Component from '@ember/component';
import {inject as injectService} from '@ember/service';

import layout from '@bennerinformatics/ember-fw-gc/templates/components/fw-doc-link';

/**
 * Documentation Link Component. This component simply links to the documentation set in GC for the current app.
 *
 * Just call the component, there is no configuration:
 * ```handlebars
 * <FwDocLink />
 * ```
 *
 * @public
 * @class FwDocLink
 * @module Components
 */
export default Component.extend({
    layout,

    tagName: 'a',

    ajax: injectService(),
    config: injectService(),

    init() {
        this._super(...arguments);
        this._loadAppData();
    },

    // we need to have the app in order to get the documentation link
    _loadAppData() {
        let appUrl = this.get('config').formGCUrl('apps', this.get('config.appId'));
        this.get('ajax').request(appUrl).then(({app}) => {
            this.set('app', app);
        });
    }
});
