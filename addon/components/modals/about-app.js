import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';
import {inject as injectService} from '@ember/service';
import {isArray as isEmberArray} from '@ember/array';
import {computed} from '@ember/object';

import layout from '@bennerinformatics/ember-fw-gc/templates/components/modals/about-app';

/**
 * The About Modal is a modal which gives very basic information about your app. This does not need to be called by you, as it is called for you by the `FwHeader`. The About Modal can be called from the dropdown to the right of the header.
 * Nothing displayed in the modal can be configured by passing information into the model attribute, but the information is all dynamic and can be configured by looking at two places: Group Control or `fw.json`.
 *
 *  Some of the information is pulled directly from Group Control, namely, the App Name, App Description and More Information Link (Link field on App model). This can hypothetically be changed per database, though
 *  it is not advised that you would change the defaults set by the initial database migration. The More Information Link is the one of these three values which is not set by default in the database migration, so that
 *  each database must edit the link field on the app to cause this link to show up.
 *
 * The rest of the information is set by the values of `fw.json`, including:
 *
 * - `version` will display under the title, and this is the current version of the app.
 * - `authors`, `maintainers`, and `previousMaintainers` should all be set as arrays in `fw.json`, and they will be displayed in the About modal under these names as a string separated by a '|' between each value
 *
 * @class AboutAppModal
 * @module Components
 */
export default BaseModal.extend({
    layout,

    ajax: injectService(),
    config: injectService(),

    authors: computed('config.authors', function () {
        let authors = this.get('config.authors') || '';
        if (isEmberArray(authors)) {
            authors = authors.map((person) => {
                return `<div class="person-name">${person}</div>`;
            });
        }

        return (isEmberArray(authors)) ? authors.join(' | ') : authors;
    }),

    maintainers: computed('config.maintainers', function () {
        let maintainers = this.get('config.maintainers') || '';
        if (isEmberArray(maintainers)) {
            maintainers = maintainers.map((person) => {
                return `<div class="person-name">${person}</div>`;
            });
        }

        return (isEmberArray(maintainers)) ? maintainers.join(' | ') : maintainers;
    }),

    previousMaintainers: computed('config.previous-maintainers', function() {
        let previousMaintainers = this.get('config.previous-maintainers') || '';
        if (isEmberArray(previousMaintainers)) {
            previousMaintainers = previousMaintainers.map((person) => {
                return `<div class="person-name">${person}</div>`;
            });
        }

        return (isEmberArray(previousMaintainers)) ? previousMaintainers.join(' | ') : previousMaintainers;
    }),

    init() {
        this._super(...arguments);
        this._loadAppData();
    },

    _loadAppData() {
        let appUrl = this.get('config').formGCUrl('apps', this.get('config.appId'));
        this.get('ajax').request(appUrl).then(({app}) => {
            this.set('app', app);
        });
    }
});
