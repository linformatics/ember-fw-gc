import {inject as injectService} from '@ember/service';
import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';
import {validator, buildValidations} from 'ember-cp-validations';
import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';
import layout from '@bennerinformatics/ember-fw-gc/templates/components/modals/re-login';

const ReloginValidation = buildValidations({
    password: validator('password', {
        dependentKeys: ['wrongPassword']
    })
});

/**
 * The re-login modal is a modal which will appear any time the session expires, and will allow you to simply put in your password to relogin, rather than taking you to
 * the login page. Each app needs to call it in the `application.hbs` file, so that it can be called on any page. It should be set to be the `sm` modal size, per our
 * Informatics modal policies, [here](https://apps.bennerlibrary.com/kb/#/entry/1205). This is what it should look like:
 *
 * ```handlebars
 * <FwFullscreenModal @modal="re-login" @size="sm" @model={{hash attemptedTransition=attemptedTransition}} @close={{action "closeLoginModal"}} />
 * ```
 *
 * The `shouldRender` boolean should be `showLoginModal`, which is the boolean which is changed by the `ApplicationControllerMixin`, so the if statement should look like:
 * ```handlebars
 * {{#if showLoginModal}}
 *      {{!-- Fullscreen Modal Re-Login information here --}}
 * {{/if}}
 * ```
 * Then, so long as you use the [ApplicationControllerMixin](../classes/ApplicationControllerMixin.html) as directed on your `application.js` controller, everything should work properly.
 * This should be set up for you by the Generator FW, so you should already have these things set up for you in your app, but it is important to note that the re-login modal is the one
 * modal which needs to be called in the application template by each app.
 *
 * @class ReLoginModal
 * @module Components
 */

export default BaseModal.extend(ReloginValidation, ValidationMixin, {
    layout,
    password: '',

    wrongPassword: false,
    noLogin: false,

    config: injectService(),
    currentUser: injectService(),
    session: injectService(),
    ajax: injectService(),
    notifications: injectService(),

    close() {
        this.get('session').trigger('after-re-login');
        this.closeModal();
    },

    actions: {
        /** Logs in again using the user's password */
        confirm() {
            this.get('notifications').closeAllNotifications('re-login');
            let user = this.get('currentUser');
            let data = JSON.stringify({
                user: user.get('userId'),
                pass: this.get('password'),
                app: this.get('config.appId')
            });
            this.set('wrongPassword', false);
            return this.get('ajax').post(this.get('config').formGCUrl('session'), {data}, true).then(() => {
                user.changeDepartment(user.get('currentDept')).then(() => {
                    let attemptedTransition = this.get('model.attemptedTransition');

                    this.set('password', '');
                    if (attemptedTransition) {
                        attemptedTransition.retry();
                    }
                    this.close();
                }).catch(() => {
                    this.close();
                });
            }).catch((e) => {
                // The only problem will be if it is a incorrect password or server error.  There is not a chance for not having access to application.
                // So simply check if it is a status code error of 500 (Internal Server Error), then it will say Failed to Connect to Database otherwise
                // It will say invalid Password
                if (e.status == 500) {
                    this.get('notifications').showError('Failed to connect to the database', 're-login');
                } else {
                    this.get('notifications').showError('Password is invalid', 're-login');
                }
                throw e;
            });
        },

        /** Retries login in case we just temporarily were logged out */
        refresh() {
            let ntf = this.get('notifications');
            ntf.closeAllNotifications('re-login');
            return this.get('ajax').request(this.get('config').formGCUrl('session', 'check')).then(({isActive}) => {
                if (isActive) {
                    let attemptedTransition = this.get('model.attemptedTransition');
                    if (attemptedTransition) {
                        attemptedTransition.retry();
                    }
                    this.close();
                } else {
                    ntf.showError('Not currently logged in, please enter a password and click confirm', 're-login');
                    this.set('noLogin', true);
                }
            }).catch((e) => {
                if (e.status == 500) {
                    ntf.showError('Failed to connect to the database', 're-login');
                } else {
                    ntf.showError('Not currently logged in, please enter a password and click confirm', 're-login');
                    this.set('noLogin', true);
                }
                return false;
            });
        },

        /** Closes the modal, logging the user out */
        closeModal() {
            if (confirm('Are you sure you want to close? You will lose all of your unsaved data!')) {
                this.get('session').invalidate();
            }
        }
    }
});
