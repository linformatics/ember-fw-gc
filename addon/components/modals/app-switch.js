import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';
import {inject as service} from '@ember/service';

import layout from '@bennerinformatics/ember-fw-gc/templates/components/modals/app-switch';

/**
 * The App Switcher Modal is a modal that allows you to switch your current app. It loads the logos and names of each app, downloaded to the server, which the current user has any role in, and clicking
 * one of those apps will open a new tab in the browser and load that app. There is no configuration for this modal, and you do not need to call it yourself, as it is automatically built into the `FwHeader`
 * component (no way to turn it off from that component either) that has no configuration necessary. The only way to configure this modal would be to change a certain users roles for certain apps in Group Control.
 *
 * @class AppSwitchModal
 * @module Components
 */
export default BaseModal.extend({
    layout,
    apps: [],

    appMeta: service(),
    currentUser: service(),

    init() {
        this._super(...arguments);
        this._loadApps();
    },

    _loadApps() {
        return this.get('appMeta').findMetas(this.get('currentUser.apps'), true).then((apps) => {
            this.set('apps', apps);
        });
    },

    actions: {
        openOrFocus(app) {
            if (!window.popups) {
                window.popups = {};
            }
            if (window.popups[app.get('name')]) {
                window.popups[app.get('name')].close();
            }
            window.popups[app.get('name')] = window.open(app.get('url'), app.get('name'));
            this.closeModal();
        }
    }
});
