import Component from '@ember/component';
import layout from '@bennerinformatics/ember-fw-gc/templates/components/fw-gc-nav';
import {inject} from '@ember/service';

/**
* This component extends [FwNav](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FwNav.html). It retains all the basic features of the navigation implemented by `FwNav`, but it also changes depending upon whether the user is authenticated
* or not. If the session is authenticated and the user has roles within the app, `FwGcNav` will yield what is passed into it into the `FwNav` component. If however, the session is authenticated and the user has no roles, the only page which will be displayed
* in the navigation is Index. If the session has not been authenticated, the Login page will be the only page displayed in the navigation bar. For more information about the login page, [click here](../classes/LoginPage.html).
*
* The usage of `FwGcNav` is identical to `FwNav`:
* ```handlebars
* <FwGcNav>
*    <ul>
*        <li>
*            <LinkTo @route="myPage">My Page</LinkTo>
*    </ul>
* </FwGcNav>
* ```
 * @class FwGcNav
 * @extends FwNav
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: '',
    config: inject(),
    currentUser: inject(),
    session: inject()
});
