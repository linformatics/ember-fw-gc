import Component from '@ember/component';
import {inject as service} from '@ember/service';
import AuthComponentMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-component';

/**
 * For Ember deprecation notice, [click here](https://deprecations.emberjs.com/id/ember-component-is-visible/). This component is used to show or hide a button
 * based on a certain permission level or department. It is similar to the same
 * function as [has-role](../classes/HasRole.html) or [has-department](../classes/HasDepartment.html) helpers,
 * except more robust, and designed only for a button itself.
 *
 * Basic Usage:
 *
 * ```handlebars
 * <AuthButton @perm="admin" @type="button" @class="btn btn-sm btn-default" @click={{action "click"}}>
 *     Click me if you're an admin!
 * </AuthBlock>
 * ```
 *
 * See the docs for [AuthComponentMixin](../classes/AuthComponentMixin.html)
 * for more details on how you can use this component.
 *
 * @public
 * @class AuthButton
 * @extends Ember.Component
 * @uses AuthComponentMixin
 * @module Components
 * @deprecated Ember Component isVisible has been deprecated by Ember. Use has-role or has-department helper instead.
 */
export default Component.extend(AuthComponentMixin, {
    tagName: 'button',
    attributeBindings: ['disabled', 'type'],

    /**
     * Whether or not to mark the button as 'disabled'
     * See <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-disabled">here</a>
     * for more details
     *
     * @public
     * @property disabled
     * @type {Boolean}
     */
    disabled: false,

    /**
     * What the type of the button is
     * See <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type">here</a>
     * for more details
     *
     * @public
     * @property type
     * @type {String}
     */
    type: 'button',

    session: service(),
    user: service('current-user'),

    /**
     * Action to be called when the button is clicked.
     *
     * @public
     * @property click
     * @type {Action}
     */
    click: () => {}
});
