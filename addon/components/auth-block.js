import Component from '@ember/component';
import AuthComponentMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-component';

/**
 * For Ember deprecation notice, [click here](https://deprecations.emberjs.com/id/ember-component-is-visible/). This component is used to show or hide various behavior
 * based on a certain permission level or department. It is a more robust way of
 * doing the same thing that is accomplished by the [has-role](../classes/HasRole.html)
 * or the [has-department](../classes/HasDepartment.html) helpers.
 *
 * Basic Usage:
 *
 * ```handlebars
 * <AuthBlock @perm="admin">
 *     {{!-- block of handlebars which will be displayed based on permissions given--}}
 * </AuthBlock>
 * ```
 *
 * See the docs for [AuthComponentMixin](../classes/AuthComponentMixin.html)
 * for more details on how you can use this component.
 *
 * @public
 * @class AuthBlock
 * @uses AuthComponentMixin
 * @module Components
 * @deprecated Ember Component isVisible has been deprecated by Ember. Use has-role or has-department helper instead.
 */
export default Component.extend(AuthComponentMixin);
