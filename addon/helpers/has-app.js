import Helper from '@ember/component/helper';
import {inject} from '@ember/service';
import match from '@bennerinformatics/ember-fw-gc/utils/match';

/**
* Checks if a user has access to a specific app. Uses the [match](../classes/MatchUtil.html)
* utility, so anything passed in for the app will be passed to the match function. The `needle`
* is a flat list of all user apps, and the `haystack` is what you pass into the component. You are
* able to check one or multiple apps, as in the example below. Note, that the
* `has-app` helper uses positional parameters to make an array, thus, your answer will always be wrapped
* in one array, so use this when formatting your checks.
*
* Usage:
 * ```handlebars
 * {{has-app 'appId'}} <!-- if the user has access to the "appId" app, this returns true -->
 * {{has-app 'appId1' 'appId2'}} <!-- if the user has access to either app, this returns true -->
 * {{has-app (array 'appId1' 'appId2')}} <!-- if the user has access to both of the apps, this returns true -->
 * ```
 *
 * @public
 * @class HasApp
 * @extends Ember.Helper
 * @module Helpers
 */
export default Helper.extend({
    currentUser: inject(),

    compute(apps) {
        return match(this.get('currentUser.apps'), apps);
    }
});
