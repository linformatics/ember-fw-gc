import Helper from '@ember/component/helper';
import {inject as service} from '@ember/service';

/**
 * Checks if a user has a specific role. Uses the [match](../classes/MatchUtil.html)
 * utility, so anything passed in for the role will be passed to the match function. The `needle`
 * is a flat list of all user roles, and the `haystack` is what you pass into the component. Thus,
 * you are able to pass an and/or array into the this for more complicated role checks. Note, that the
 * `has-role` helper uses positional parameters to make an array, thus, your answer will always be wrapped
 * in one array, so use this when formatting your checks.
 *
 * Usage:
 * ```handlebars
 * <!-- if the match function returns true this will return true -->
 * {{has-role 'admin'}} <!-- Checks for 'admin' role -->
 * {{has-role 'admin' 'stats'}} <!-- Checks for 'admin' OR 'stats' role -->
 * {{has-role (array 'admin' 'stats')}} <!-- Checks for 'admin' AND 'stats' role -->
 * {{has-role 'admin' (array 'supervisor' 'stats')}} <!-- Checks for 'admin' OR ('supervisor' AND 'stats')-->
 * ```
 *
 * @public
 * @class HasRole
 * @extends Ember.Helper
 * @module Helpers
 */
export default Helper.extend({
    currentUser: service(),

    compute(role) {
        return this.get('currentUser').match(role);
    }
});
