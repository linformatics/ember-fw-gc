import Helper from '@ember/component/helper';
import {inject as injectService} from '@ember/service';

/**
 * This helper can be used to determine if the user is part of a specific department.
 * It does *not* extend match, so it will not take an either/or array. It will only take
 * a single string. Then the helper returns a boolean value based on whether or not the
 * user is in the department.
 *
 * Usage:
 * ```handlebars
 * {{has-department 'test'}} <!-- if the user is in the "test" department, this returns true -->
 * ```
 *
 * @public
 * @class HasDepartment
 * @extends Ember.Helper
 * @module Helpers
 */
export default Helper.extend({
    currentUser: injectService(),

    compute(department) {
        return this.get('currentUser.departments').includes(department.get('id'));
    }
});
