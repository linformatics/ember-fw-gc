import FWAdapter from '@bennerinformatics/ember-fw/adapters/fw';

/**
 * Extension of the [base FW adapter](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FwAdapter.html) for models that need to be fetched from a dependency app. Currently, this app adapter should only be used for Group Control models,
 * since that is the only app that is a dependency app, thus the appId is automatically set to `gc`. Most often this will be used for the GC user model, so you need to make an adapter file, `adapters/user.js`,
 * and then that file is merely set to:
 *
 * ```js
 * export {default} from '@bennerinformatics/ember-fw-gc/adapters/fw-app';
 * ```
 *
 * Provided you have the model file setup right (as documented in the [user model](UserModel.html)), it should now pull the data from GC app instead of your base app.
 *
 * @class FwAppAdapter
 * @extends FwAdapter
 * @module Miscellaneous
 */
export default FWAdapter.extend({
    /**
     * ID of the app containing this model
     * @type {string}
     * @prop appId
     * @default 'gc'
     */
    appId: 'gc',

    pathForType(type) {
        // simply append appId to the app name
        let model = this._super(type);
        return `${this.get('appId')}/${model}`;
    }
});
