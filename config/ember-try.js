/* eslint-env node */
module.exports = {
    bowerOptions: ['--allow-root=true'],
    command: 'ember test --filter="ESLint" -i',
    useYarn: true,
    scenarios: [
        {name: 'default'},
        {name: 'ember-release'},
        {name: 'ember-beta'},
        {name: 'ember-canary'}
    ]
};
