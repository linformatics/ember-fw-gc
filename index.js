/* eslint-env node */
/* eslint-disable object-shorthand */
'use strict';

module.exports = {
    name: '@bennerinformatics/ember-fw-gc',
    isDevelopingAddon: () => true
};
