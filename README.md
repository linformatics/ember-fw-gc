# ember-fw-gc

We have a couple of documentation websites, which may help in using this package:

[Ember FW GC Docs](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-gc) give a more robust guide to some of the important features of the package.

[Ember FW GC API Docs](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw-gc) are api documentation for each of our components, which are generated from code comments.

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone` this repository
* `npm install`

## Running

* `ember serve`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://ember-cli.com/](http://ember-cli.com/).
