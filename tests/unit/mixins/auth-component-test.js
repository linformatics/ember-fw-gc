/* jshint expr:true */
import {expect} from 'chai';
import {describe, beforeEach, it} from 'mocha';
import EmberObject from '@ember/object';
import AuthComponentMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-component';

describe('AuthComponentMixin', function () {
    it('works', function () {
        let AuthComponentObject = EmberObject.extend(AuthComponentMixin);
        let subject = AuthComponentObject.create();

        expect(subject).to.be.ok;
    });

    it('returns false if session is not authenticated', function () {
        let AuthComponentObject = EmberObject.extend(AuthComponentMixin, {
            session: {
                isAuthenticated: false
            },
            currentUser: {}
        });
        let subject = AuthComponentObject.create();

        expect(subject.get('session.isAuthenticated')).to.be.false;
        expect(subject.get('isVisible')).to.be.false;
    });

    describe('when logged in', function () {
        let AuthComponentObject;

        beforeEach(function () {
            AuthComponentObject = EmberObject.extend(AuthComponentMixin, {
                session: {
                    isAuthenticated: true
                },
                currentUser: {}
            });
        });

        it('returns correct defaultVisibility if nothing is specified', function () {
            let subject = AuthComponentObject.create();

            subject.set('defaultVisibility', false);
            expect(subject.get('isVisible')).to.be.false;

            subject.set('defaultVisibility', true);
            expect(subject.get('isVisible')).to.be.true;
        });

        it('returns true if override works correctly', function () {
            let subject = AuthComponentObject.create({
                override: 'test-role',
                currentUser: {
                    match(role) {
                        return role === 'test-role';
                    }
                }
            });

            expect(subject).to.be.ok;
            expect(subject.get('isVisible')).to.be.true;
        });
    });
});
