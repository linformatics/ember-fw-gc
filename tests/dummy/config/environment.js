/* jshint node: true */

var pkg = require('../../../package.json');

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'dummy',
        environment,
        rootURL: '/',
        locationType: 'hash',
        EmberENV: {
            FEATURES: {
            // Here you can enable experimental features on an ember canary build
            // e.g. 'with-controller': true
            }
        },

        APP: {
            name: 'FW Ember (GC)',
            version: pkg.version,
            config: {
                name: 'FW Ember (GC)',
                version: pkg.version,
                api: 'api/v0.1',
                logo: 'assets/images/logo.png',
                developers: ['Austin Burdine']
            }
        }
    };

    // if (environment === 'development') {
    //     // ENV.APP.LOG_RESOLVER = true;
    //     // ENV.APP.LOG_ACTIVE_GENERATION = true;
    //     // ENV.APP.LOG_TRANSITIONS = true;
    //     // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    //     // ENV.APP.LOG_VIEW_LOOKUPS = true;
    // }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
    }

    if (environment === 'production') {
        ENV['ember-cli-mirage'] = {
            enabled: true
        };
        ENV.baseURL = '/docs/fw-ember-gc/';
    }

    return ENV;
};
