import EmberRouter from '@ember/routing/router';
import config from './config/environment';
import authRouter from '@bennerinformatics/ember-fw-gc/router';

const Router = EmberRouter.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

Router.map(function () {
    authRouter(this);

    this.route('installation');
    this.route('basic-components', function () {
        this.route('content-box');
        this.route('notifications');
        this.route('tooltip-popover');
    });

    this.route('cookbook', function () {
        this.route('ember-power-select');
        this.route('ember-load');
    });
});

export default Router;
