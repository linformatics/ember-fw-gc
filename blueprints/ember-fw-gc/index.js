/* eslint-env node */
/* eslint-disable object-shorthand,no-var */
module.exports = {
    normalizeEntityName: function () {},

    afterInstall: function () {
        var that = this;

        return that.addAddonToProject({name: 'ember-simple-auth', target: '1.1.0'});
    }
};
