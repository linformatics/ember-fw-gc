import PromiseButton from '@bennerinformatics/ember-fw/components/fw-promise-button';
import AuthComponentMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-component';

export default PromiseButton.extend(AuthComponentMixin);
