import AuthComponent from '@bennerinformatics/ember-fw-gc/mixins/auth-component';
import LinkComponent from '@ember/routing/link-component';

export default LinkComponent.extend(AuthComponent);
