import AuthRoute from '@bennerinformatics/ember-fw-gc/routes/auth';
import AuthRouteMixin from '@bennerinformatics/ember-fw-gc/mixins/auth-route';

/**
 * Convenience route that bundles some of the auth route mixins. It essentially handles redirects and everything to ensure that only users who are authenticated can
 * access the route. It also handles redirecting to index route if the user has no roles within the app.
 *
 * Usage: (in a routes file)
 * ```javascript
 * import AuthRoute from './auth';
 *
 * export default AuthRoute.extend({
 *     // your code here...
 * });
 * ```
 * Basically all of the routes in your app should extend `AuthRoute` (or `RestrictedRoute` if only certain roles have access to that page) unless you want the route to be accessible
 * even if the user is not logged in.
 *
 * @class AuthRoute
 * @extends AuthRoute-Addon
 * @uses AuthRouteMixin
 * @module Miscellaneous
 */
export default AuthRoute.extend(AuthRouteMixin);
