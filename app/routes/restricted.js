import AuthRoute from '@bennerinformatics/ember-fw-gc/routes/auth';
import RestrictedRouteMixin from '@bennerinformatics/ember-fw-gc/mixins/restricted-route';

/**
 * Convenience route that bundles some of the restricted route mixins. It extends the basic functionality of the AuthRoute, except it adds to it one more factor:
 * it allows you to filter which roles have access to this page. It also allows you to specify a `rejectedRoute` to transition to when the user does not have the
 * proper roles. This is used most commonly within the apps for the admin pages (which are restricted to `admin` and `global-admin`) roles. For more information see
 * the RestrictedRouteMixin.
 *
 * Usage: (in a routes file)
 * ```javascript
 * import RestrictedRoute from './restricted';
 *
 * export default RestrictedRoute.extend({
 *     // your code here...
 * });
 * ```
 * @class RestrictedRoute
 * @extends AuthRoute-Addon
 * @uses RestrictedRouteMixin
 * @module Miscellaneous
 */
export default AuthRoute.extend(RestrictedRouteMixin);
