import SessionService from 'ember-simple-auth/services/session';

export default SessionService.extend({
    triggerReAuthenticate(transition = null) {
        this.trigger('re-login', transition);
    }
});
