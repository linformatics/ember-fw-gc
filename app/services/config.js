import env from '../config/environment';
import ConfigService from '@bennerinformatics/ember-fw-gc/services/config';

const {APP: {config}} = env;

export default ConfigService.extend(config);
